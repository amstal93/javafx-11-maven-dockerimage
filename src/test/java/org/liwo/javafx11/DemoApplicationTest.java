package org.liwo.javafx11;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;

@ExtendWith(ApplicationExtension.class)
class DemoApplicationTest extends ApplicationTest {

	@Start
	public void start(final Stage stage) {
		stage.setScene(setupScene());
		stage.show();
	}

	private Scene setupScene() {
		final Label label = new Label();

		Button button = new Button();
		button.setText("Say 'Hello World'");
		button.setOnAction(event -> label.setText("Hello World!"));

		FlowPane root = new FlowPane();
		root.getChildren().addAll(button, label);

		return new Scene(root, 300, 250);
	}

	@Test
	void labelIsChanged() {
		clickOn(".button");

		verifyThat(".label", hasText("Hello World!"));
	}
}